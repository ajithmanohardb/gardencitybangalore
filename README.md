##About Autistica App

The mobile application is built using reactnative. We have covered two users - child and parent.
The UI has two modules:
* child module - the child module has voice capture feature integrated which enables the user to interact and the text is captured and sent to backend api 
* to process. 
* parent module - The parent module is intended for the parent user to monitor child's behaviour .

* UI and Backend are not integrated. Currently mock data is being used instead of backend services.
* Backend is rest web services.

* checkout [For backend API setup](https://bitbucket.org/ajithmanohardb/gardencitybangalore/src/46c2a4198364858133391fb114cd24e9630eb487/api/README.md?at=master&fileviewer=file-view-default)

* [For UI Setup](https://bitbucket.org/ajithmanohardb/gardencitybangalore/src/72ea85c3391e75eafce4d22a4f0b6cc1aceb4c6a/app/README.md?at=master&fileviewer=file-view-default)