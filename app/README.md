# Autistica UI
autistica UI

## Pre-requisites

* Node.js, Watchman, the React Native command line interface, Java and Android Studio.

## Running the API
Node 
* Node.js and Python2 can be installed via Chocolatey, a popular package manager for Windows. Open a Command Prompt as Administrator, then run:

* choco install nodejs.install
* choco install python2

* The React Native CLI 
* Node.js comes with npm, which lets us install the React Native command line interface.

* Install Android Studio 

* Android Studio provides the Android SDK and AVD (emulator) required to run and test your React Native apps.

* Install the AVD and HAXM 

* Choose Custom installation when running Android Studio for the first time. Make sure the boxes next to all of the following are checked:

* Android SDK
* Android SDK Platform
* Performance (Intel ® HAXM)
* Android Virtual Device
* Then, click "Next" to install all of these components.


* For details on installation on various OS - MACOs, Linux, follow the below URL: - 
* http://facebook.github.io/react-native/docs/getting-started.html#content