'use strict';

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  ToastAndroid,
  TouchableHighlight,
  Navigator
} from 'react-native';

var SadPage1 = require('./SadPage1.js');

export default class HappyPage1 extends Component {
	constructor(props){
		super(props);

    this.state = {
      isSmileButtonPressed: false,
      isDiaryButtonPressed: false,
      isWatchVidoButtonPressed: false
    };
	  
  };

	render () {
    var message = 'What makes you Happy ?';

		 return (
            <View style={styles.container}>            
                <View style={{height: 90, backgroundColor: '#\D6A2DC'}}></View>
              

                 <Text style={styles.welcome}>
                        {message}
                  </Text>

                       <TouchableHighlight 
                          onPress={this.onSmileIconPressed.bind(this)}
                          style={styles.smile}>
                             <Image
                                style={styles.logo}
                                source={require('./images/emoticons/MicButton.png')}
                              />
                       </TouchableHighlight>
                    
                   <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between'}}>                          
                        <TouchableHighlight 
                          onPress={this.onDiaryIconPressed.bind(this)}
                          style={styles.diaryButton}>
                             <Text style={styles.DairyButtonText}> See Diary </Text>
                       </TouchableHighlight>

                        <TouchableHighlight 
                          onPress={this.onWatchVideoIconPressed.bind(this)}
                          style={styles.videoButton}>
                             <Text style={styles.DairyButtonText}> Watch Videos </Text>
                       </TouchableHighlight>
                  </View>

             </View> 
      );
	}

  onSmileIconPressed(){       
      this.setState({isSmileButtonPressed: true});
  }

  onDiaryIconPressed(){       
      this.setState({isDiaryButtonPressed: true});
  }

  onWatchVideoIconPressed(){       
      this.setState({isWatchVidoButtonPressed: true});
  }  
};


const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch',
    backgroundColor: '#FCECFC',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
    paddingTop: 30
  },
  logo: {
    width: 100,
    height: 100,
  },
  smile: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 80,
    backgroundColor: '#FCECFC',
    alignSelf: 'center'
  },
  DairyButtonText: {
    color: '#fff',
    fontSize: 20,
    alignSelf: 'center',
},
diaryButton: {
        height: 60,
        width: 150,
        borderRadius: 10,
        backgroundColor: '#48BBEC',
        borderColor: '#48BBEC',
        alignSelf: 'center',
        marginTop: 10,
        justifyContent: 'center',
        alignItems: 'flex-end',
        paddingRight: 10,
        marginLeft: 20, 
    },
  videoButton: {
        height: 60,
        width: 150,
        borderRadius: 10,
        backgroundColor: '#48BBEC',
        borderColor: '#48BBEC',
        alignSelf: 'center',
        marginTop: 10,
        justifyContent: 'center',
        alignItems: 'flex-end',
        paddingLeft: 10,
        marginRight: 20, 
   },

});

module.exports = HappyPage1; 