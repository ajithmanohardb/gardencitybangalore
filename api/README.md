# Autistica REST Service API
autistica Rest API Service

## Pre-requisites

* NodeJS, SailsJs, MongoDB.
* Additionally need supertest mocha lib to be globally installed.

## Database Setup
* Database setup will automatically creates when app is running


## Running the API
* npm install
* sails lift
* Now open up url http://localhost:1337/api-docs, to view the API docs in Swagger for view / testing.
* APIs are documented in Swagger


## Deployment Details
* Currently for development version is running on local

## Integration with another services
* Authorization process
* Call flow
