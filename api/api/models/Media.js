var uuid = require('node-uuid');

module.exports = {
  attributes: {
  name: {
      type: 'string',
      required: true
    },
    description:{
      type:'string',
      required: true
    },
    type:{
       type: 'string',
       required:true
    },
    content: {
      type: 'string',
      required: true
    },
    feelings:{
      type: 'json',
      required: true,
      defaultsTo: '[]'
    },
    feelingType:{
      type: 'json',
      required: true,
      defaultsTo: '[]'
    },
    minAge:{
      type:'integer',
      required:true
    },
    maxAge:{
      type:'integer',
      required:true
    },
    gender:{
      type:'string',
      required: true
    },
    meta:{
      type: 'json',
      required: true,
      defaultsTo: '[]'
    },
    toJSON: function () {
      var obj = this.toObject();
      delete obj.createdAt;
      return obj;
    }
  },
};