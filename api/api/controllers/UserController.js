/**
 * UserController
 *
 * @description :: Server-side logic for managing UserController
 * @author      :: Santosh Kumar Talachutla
 */

module.exports = {

  get: function (req, res) {
    var params = req.params.all();
    UserService.get(params, req.profile, function (err, records) {
      if (err)
        return res.notFound(err);
      return res.ok(records);
    });
  },

  create: function (req, res) {
    var params = req.params.all();
    UserService.create(params, req.profile, function (err, records) {
      if (err)
        return res.notFound(err);
      return res.ok(records);
    });
  },

update: function (req, res) {
    var params = req.params.all();
    UserService.update(params, req.profile, function (err, records) {
      if (err)
        return res.notFound(err);
      return res.ok(records);
    });
  },

  destroy: function (req, res) {
    var params = req.params.all();
    UserService.remove(params, req.profile, function (err, records) {
      if (err)
        return res.notFound(err);
      return res.ok(records);
    });
  },

};